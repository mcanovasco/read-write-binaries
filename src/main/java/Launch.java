import java.io.*;
import java.net.URL;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

public class Launch {

    private static final int DNI_BYTES = 9;

    private static final int NOM_TREBALLADOR_BYTES = 16;

    private static final int COGNOM_TREBALLADOR_BYTES = 16;

    private static final int SEGON_COGNOM_TREBALLADOR_BYTES = 16;

    private File carpetaRegitresIndividuals = new File("registres_individuals");

    private void registros() throws FileNotFoundException, IOException {
        if (!carpetaRegitresIndividuals.exists()) {
            carpetaRegitresIndividuals.mkdir();
        }

        File file = new File("dades/entradesSortides.txt");

        FileReader fr = new FileReader(file);
        BufferedReader br = new BufferedReader(fr);
        String line;
        String fecha = "";
        while ((line = br.readLine()) != null) {
            String[] items = line.split("#");
            String dni = items[3];

            if (file.length() == 0 || !fecha.equals(items[1])) {
                fecha = items[1];
                anadirRegistros(fecha);
            }
            procesarEntradaSalida(items[0], items[2], dni);

        }

    }

    private void procesarEntradaSalida(String marcador, String hora, String dni) throws IOException {
        RandomAccessFile raf = new RandomAccessFile(new File(carpetaRegitresIndividuals, dni + ".bin"), "rw");
        raf.seek(raf.length() - 40);
        raf.writeChars("N");
        if (marcador.equals("S")) {
            byte[] buffer = new byte[16];
            raf.read(buffer);
            String entrada = new String(buffer, "UTF-16");
            raf.writeChars(hora);
            if (!entrada.trim().isEmpty()) {
                raf.writeChars(String.valueOf(minutosEntradaSalida(entrada, hora)));
                raf.writeChars("\r\n");
            }

        } else {
            raf.writeChars(hora);
        }
        raf.close();

    }

    private long minutosEntradaSalida(String entrada, String salida) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");
        LocalTime timeEntrada = LocalTime.parse(entrada, dateTimeFormatter);
        LocalTime timeSalida = LocalTime.parse(salida, dateTimeFormatter);
        long minutes = timeEntrada.until(timeSalida, ChronoUnit.MINUTES);
        return minutes;
    }

    private void anadirRegistros(String fecha) throws FileNotFoundException, IOException {
        File file = new File("dades/treballadors.bin");

        FileInputStream fis = new FileInputStream(file);
        InputStreamReader isr = new InputStreamReader(fis, "UTF-16");
        BufferedReader br = new BufferedReader(isr);

        char[] buffer = new char[DNI_BYTES];
        while (br.read(buffer) != -1) {
            String dni = new String(buffer);
            RandomAccessFile raf = new RandomAccessFile(new File(carpetaRegitresIndividuals, dni + ".bin"), "rw");
            raf.seek(raf.length());
            raf.writeChars(fecha + "S" + "        " + "        " + "0" + "\r\n");
            raf.close();

            br.skip(NOM_TREBALLADOR_BYTES + COGNOM_TREBALLADOR_BYTES + SEGON_COGNOM_TREBALLADOR_BYTES);
        }

        br.close();
        isr.close();
        fis.close();

    }

    public static void main(String[] args) {
        Launch launch = new Launch();
        try {
            launch.registros();
        } catch (IOException e) {
            System.out.println("Problems processing the files");
        }
    }

}
